# FABRIK algorithm implementation
## :lizard: Area of expertise: Computer Graphics

- Read more about FABRIK: [Paper](http://www.andreasaristidou.com/publications/papers/FABRIK.pdf)
- Find more about this project: [3DReptiles website](http://3dreptiles.cs.ucy.ac.cy/)

As specified in the paper above regarding FABRIK, *"FABRIK uses a forwardand backward iterative approach, finding each joint posi-tion via locating a point on line. FABRIK has been utilisedin highly complex systems with single and multiple tar-gets, with and without joint restrictions. It can easily han-dle end effector orientations and support, to the best of ourknowledge, all chain classes. A reliable method for incorpo-rating constraints is also presented and utilised within FABRIK"*

![Lizard labeling process](images/lizard2.PNG "Lizard labeling process")

## :file_folder: Current repository
This repository contains the:
- Implementation of FABRIK for a lizard with 17 markers

Based on **Andreas Aristidou's** snake implementation, I converted the code to python and implemented a more complex than snake model, the lizard.

## :dart: Goals
- Parse a C3D file
- Read the markers' values and fix **missing** values
- Export **fixed** values as a new C3D file
- We should be able to use the new file in order to apply a lizard model and export realistic animations

## :computer: Implementation
- The lizard we would like to fix the values for has in general the following marker labels: 
  - Lizard:Head
  - Lizard:Neck
  - Lizard:FrontRightLeg
  - Lizard:FrontRightFoot
  - Lizard:FrontLeftFoot
  - Lizard:FrontLeftLeg
  - Lizard:UpperBack or UpperBody
  - Lizard:LowerBack or LowerBody
  - Lizard:BackRightLeg
  - Lizard:BackRightFoot
  - Lizard:BackLeftLeg
  - Lizard:BackLeftFoot
  - Lizard:Tail0
  - Lizard:Tail1
  - Lizard:Tail2
  - Lizard:Tail3
  - Lizard:Tail4

## Dependencies
- Numpy
- C3D server(python bindings)
- Matplotlib

Other official modules
- Math
- Argparse
- Os
## How to use:
#### Windows or Python version 3.8^
```python
py fix_lizard.py <c3d_filename> 
```
#### Linux, Max or Python version <3.8
```python
python fix_lizard.py <c3d_filename>
```

eg. for Windows 10, python 3.8 version
```python
py fix_lizard.py Take_Lizard_03042019.c3d
```

![Lizard run example](images/lizard1.PNG "Lizard run example")
#### Obsolete parameters
- With distances(w-distances): Depreceated, algorithm calculates distances before executing FABRIK fix marker values algorithm


