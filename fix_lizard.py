import numpy as np
import pyc3dserver as c3d
import math as math
import matplotlib.pyplot as plt
import argparse
import os

def calculate_frame_missings(kin_chain, no_joints):
	""" Finds all positions where the value is missing or invalid """

	no_frames = len(kin_chain[0])
	missings = np.zeros((no_frames, no_joints))

	for i in range(no_frames):
		for j in range(no_joints):
			if(not is_empty(kin_chain[j][i])):
				missings[i][j] = False
			else:
				missings[i][j] = True

	return missings

def calculate_distance(marker1, marker2):
	""" Calculates a 3d distance between two markers """

	# If both markers are empty then the distance value should be zero
	# to indicate that the distance is incorrect
	if(is_empty(marker1) or is_empty(marker2)):
		return 0.0

	# Calculate the distance for each axis
	dist_x = marker1[0] - marker2[0]
	dist_y = marker1[1] - marker2[1]
	dist_z = marker1[2] - marker2[2]

	# Calculate overall Eucledean distance
	dist_overall = math.sqrt(dist_x * dist_x + dist_y * dist_y + dist_z * dist_z)
	
	return dist_overall

def is_empty(marker):
	""" Checks whether marker's data are all equal to zero"""

	if marker[0] == 0.0 and marker[1] == 0.0 and marker[2] == 0.0:
		return True
	return False

def calculate_all_distances(kin_chain, no_joins):
	""" Calculates the distance between consecutive markers for all frames """

	# Find the number of frames and initialize an empty array to hold the distances
	no_frames = len(kin_chain[0])
	distances = np.zeros((no_frames, no_joins-1))

	for i in range(no_frames):
		for j in range(no_joins - 1):
			distances[i][j] = calculate_distance(kin_chain[j][i], kin_chain[j+1][i])

	return distances

def vector_points_on_line(X, Y, d1, d2):
	""" 
	Implementation of the function as specified in the paper of Dr. Andreas Aristidou
	that fixes an incorrect marker
	"""

	if(d2 == 0.0 or d2 == 0):
		return X
	theLambda = (-1)*d1/d2
	return (1 + theLambda)*X - theLambda*Y

def calculate_new_position_no_tail(kin_chain, frame, marker_ind):
	""" Calculates a new position for a marker that belongs to the scenario of a missing tail """

	r0 = calculate_distance(kin_chain[marker_ind][frame], kin_chain[marker_ind + 1][frame-1])
	r1 = calculate_distance(kin_chain[marker_ind][frame-1], kin_chain[marker_ind + 1][frame-1])
	return vector_points_on_line(kin_chain[marker_ind][frame],kin_chain[marker_ind + 1][frame-1], r1, r0)

def calculate_new_position_no_head(kin_chain, frame, marker_ind):
	""" Calculates a new position for a marker that belongs to the scenario of a missing head """

	r0 = calculate_distance(kin_chain[marker_ind][frame], kin_chain[marker_ind - 1][frame-1])
	r1 = calculate_distance(kin_chain[marker_ind][frame-1], kin_chain[marker_ind - 1][frame-1])
	return vector_points_on_line(kin_chain[marker_ind][frame],kin_chain[marker_ind - 1][frame-1], r1, r0)

def has_missing_indices(missings, frame, no_joins):
	""" 
	Checks and returns whether between a set of markers of the same frame, a missing indice exists 
	Returns: boolean
	"""

	for j in range(no_joins):
		if(missings[frame][j]):
			return True
	return False

def print_matrix(matrix, rows):
	"""
	Not used in the algorithm. Strictly a testing function that prints a matrix
	"""

	for i in range(rows):
		print(matrix[i])

def FABRIK_marker_correction(kin_chain, no_joins):
	"""
	Contains the implementation of the FABRIK algorithm 
	for a spine. The implementation is based on Dr. Andreas Aristiou's(FABRIK founder)
	snake marker correction
	"""

	# Initialize numpy arrays for distanes, missing indices and calculate number of frames
	distances = calculate_all_distances(kin_chain, no_joins)
	missings = calculate_frame_missings(kin_chain, no_joins)
	no_frames = len(kin_chain[0])	

	# Starting from the second frame(index = 1) iterate all frames
	for i in range(1, no_frames):
		
		""" 
		The FABRIK algorithm checks 2 main scenarios. The scenario where the tail is missing
		and the scenario where the head is missing. If markers belong to one of those 2 scenarios,
		a different fix-value approach is used 
		"""

		# Case where tail is not visible
		if(is_empty(kin_chain[no_joins -1][i])):
			
			# Find first valid marker from the tail
			if(not is_empty(kin_chain[no_joins - 2][i])):
				kin_chain[no_joins - 1][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 2)
				missings[i][no_joins - 1] = False

			elif(not is_empty(kin_chain[no_joins - 3][i])):
				kin_chain[no_joins - 2][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 3)
				kin_chain[no_joins - 1][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 2)
				missings[i][no_joins - 1] = False
				missings[i][no_joins - 2] = False

			elif(not is_empty(kin_chain[no_joins - 4][i])):
				kin_chain[no_joins - 3][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 4)
				kin_chain[no_joins - 2][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 3)
				kin_chain[no_joins - 1][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 2)
				missings[i][no_joins - 1] = False
				missings[i][no_joins - 2] = False
				missings[i][no_joins - 3] = False

			elif(not is_empty(kin_chain[no_joins - 5][i])):
				kin_chain[no_joins - 4][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 5)
				kin_chain[no_joins - 3][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 4)
				kin_chain[no_joins - 2][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 3)
				kin_chain[no_joins - 1][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 2)
				missings[i][no_joins - 1] = False
				missings[i][no_joins - 2] = False
				missings[i][no_joins - 3] = False
				missings[i][no_joins - 4] = False

		# Case where head is not visible
		if( is_empty(kin_chain[0][i])):

			# Find first valid marker from the head
			if(not is_empty(kin_chain[1][i])):
				kin_chain[0][i] = calculate_new_position_no_head(kin_chain, i, 1)
				missings[i][0] = False
			
			elif(not is_empty(kin_chain[2][i])):
				kin_chain[1][i] = calculate_new_position_no_head(kin_chain, i, 2)
				kin_chain[0][i] = calculate_new_position_no_head(kin_chain, i, 1)
				missings[i][0] = False
				missings[i][1] = False

			elif(not is_empty(kin_chain[3][i])):
				kin_chain[2][i] = calculate_new_position_no_head(kin_chain, i, 3)
				kin_chain[1][i] = calculate_new_position_no_head(kin_chain, i, 2)
				kin_chain[0][i] = calculate_new_position_no_head(kin_chain, i, 1)
				missings[i][0] = False
				missings[i][1] = False
				missings[i][2] = False

			elif(not is_empty(kin_chain[4][i])):
				kin_chain[3][i] = calculate_new_position_no_head(kin_chain, i, 4)
				kin_chain[2][i] = calculate_new_position_no_head(kin_chain, i, 3)
				kin_chain[1][i] = calculate_new_position_no_head(kin_chain, i, 2)
				kin_chain[0][i] = calculate_new_position_no_head(kin_chain, i, 1)
				missings[i][0] = False
				missings[i][1] = False
				missings[i][2] = False
				missings[i][3] = False

			elif(not is_empty(kin_chain[5][i])):
				kin_chain[4][i] = calculate_new_position_no_head(kin_chain, i, 5)
				kin_chain[3][i] = calculate_new_position_no_head(kin_chain, i, 4)
				kin_chain[2][i] = calculate_new_position_no_head(kin_chain, i, 3)
				kin_chain[1][i] = calculate_new_position_no_head(kin_chain, i, 2)
				kin_chain[0][i] = calculate_new_position_no_head(kin_chain, i, 1)
				missings[i][0] = False
				missings[i][1] = False
				missings[i][2] = False
				missings[i][3] = False
				missings[i][4] = False

		# By this point head and tail exist
		# Find whether missing markers still occur
		if(has_missing_indices(missings, i, no_joins)):
			
			# If a marker is missing the algorithm will iterate 6 times
			kin_chain_original = np.copy(kin_chain)

			for ntimes in range(6):

				# Forward step
				for k in range(no_joins - 1):
					r0 = calculate_distance(kin_chain[k][i], kin_chain[k + 1][i-1])
					r1 = calculate_distance(kin_chain[k][i-1], kin_chain[k + 1][i-1])
					
					if(r0 == 0.0 or r0 == 0):
						kin_chain[k][i] = kin_chain[k][i - 1] 
						r0 = calculate_distance(kin_chain[k][i], kin_chain[k + 1][i-1])

					kin_chain[k + 1][i] = vector_points_on_line(kin_chain[k][i], kin_chain[k+1][i-1], r1, r0)
					if(not missings[i][k+1]):
						kin_chain[k + 1][i] = kin_chain_original[k + 1][i]
					kin_chain_original[k+1][i] = kin_chain[k+1][i]

				# Backward step
				for k in range(no_joins - 1, 0, -1):
					r0 = calculate_distance(kin_chain[k][i], kin_chain[k - 1][i-1])
					r1 = calculate_distance(kin_chain[k][i-1], kin_chain[k - 1][i-1])
					kin_chain[k - 1][i] = vector_points_on_line(kin_chain[k][i], kin_chain[k-1][i-1], r1, r0)

					if(not missings[i][k-1]):
						kin_chain[k - 1][i] = kin_chain_original[k - 1][i]
					kin_chain_original[k-1][i] = kin_chain[k-1][i]
	return kin_chain

def FABRIK_marker_correction_leg(kin_chain, no_joins):
	"""
	Contains the implementation of the FABRIK algorithm 
	for a leg. The implementation is based on Dr. Andreas Aristiou's(FABRIK founder)
	snake marker correction
	"""

	# Initialize numpy arrays for distanes, missing indices and calculate number of frames
	distances = calculate_all_distances(kin_chain, no_joins)
	missings = calculate_frame_missings(kin_chain, no_joins)
	no_frames = len(kin_chain[0])	

	# Starting from the second frame(index = 1) iterate all frames
	for i in range(1, no_frames):

		""" 
		The FABRIK algorithm checks 2 main scenarios. The scenario where the tail is missing
		and the scenario where the head is missing. If markers belong to one of those 2 scenarios,
		a different fix-value approach is used 
		"""
		
		# Case where tail is not visible
		if(is_empty(kin_chain[no_joins -1][i])):
			
			if(not is_empty(kin_chain[no_joins - 2][i])):
				kin_chain[no_joins - 1][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 2)
				missings[i][no_joins - 1] = False

			elif(not is_empty(kin_chain[no_joins - 3][i])):
				kin_chain[no_joins - 2][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 3)
				kin_chain[no_joins - 1][i] = calculate_new_position_no_tail(kin_chain, i, no_joins - 2)
				missings[i][no_joins - 1] = False
				missings[i][no_joins - 2] = False

		# By this point head and tail exist
		# Find whether missing markers still occur
		if(has_missing_indices(missings, i, no_joins)):
			
			# If a marker is missing the algorithm will iterate 6 times
			kin_chain_original = np.copy(kin_chain)

			for ntimes in range(6):

				# Forward step
				for k in range(no_joins - 1):
					r0 = calculate_distance(kin_chain[k][i], kin_chain[k + 1][i-1])
					r1 = calculate_distance(kin_chain[k][i-1], kin_chain[k + 1][i-1])

					if(r0 == 0.0 or r0 == 0):
						kin_chain[k][i] = kin_chain[k][i - 1] 
						r0 = calculate_distance(kin_chain[k][i], kin_chain[k + 1][i-1])

					kin_chain[k + 1][i] = vector_points_on_line(kin_chain[k][i], kin_chain[k+1][i-1], r1, r0)
					
					if(not missings[i][k+1]):
						kin_chain[k + 1][i] = kin_chain_original[k + 1][i]
					kin_chain_original[k+1][i] = kin_chain[k+1][i]

				# Backward step
				for k in range(no_joins - 1, 0, -1):
					r0 = calculate_distance(kin_chain[k][i], kin_chain[k - 1][i-1])
					r1 = calculate_distance(kin_chain[k][i-1], kin_chain[k - 1][i-1])
					kin_chain[k - 1][i] = vector_points_on_line(kin_chain[k][i], kin_chain[k-1][i-1], r1, r0)

					if(not missings[i][k-1]):
						kin_chain[k - 1][i] = kin_chain_original[k - 1][i]
					kin_chain_original[k-1][i] = kin_chain[k-1][i]
	return kin_chain						


# Main
def main():
	parser = argparse.ArgumentParser(description='Find the distances between the markers, for all frames of a given c3d file')
	parser.add_argument("c3d_file", help="The c3d file to iterate through")
	args = parser.parse_args()

	# Read c3d
	itf = c3d.c3dserver()
	ret = c3d.open_c3d(itf, args.c3d_file)
	dict_markers = c3d.get_dict_markers(itf)

	# Get all markers
	markers = dict_markers['DATA']['POS']
	index = 0
	initial_position = []

	# Iterate all markers and print some useful for the programmer statistics
	marker_index = 0
	print('Filename: ', args.c3d_file)
	print('Number of markers: ' + str(len(markers)))

	# Create a marker name holder list
	marker_names = []
	for marker in markers:
		marker_names.append(marker)
		print(marker)

	# Generate an empty array that holds all distances
	no_frames = len(dict_markers['DATA']['POS'][marker_names[0]])
	print('Number of frames: ' , no_frames)
	print('=============================================')

	# Save markers' data in seperate variables
	x1 = markers['Lizard:Head']
	x2 = markers['Lizard:Neck']

	if('Lizard:UpperBack' in marker_names):
		x3 = markers['Lizard:UpperBack']
	else:
		x3 = markers['Lizard:UpperBody']
	if('Lizard:LowerBack' in marker_names):
		x4 = markers['Lizard:LowerBack']
	else:
		x4 = markers['Lizard:LowerBody']
	x5 = markers['Lizard:Tail0']
	x6 = markers['Lizard:Tail1']
	x7 = markers['Lizard:Tail2']
	x8 = markers['Lizard:Tail3']
	x9 = markers['Lizard:Tail4']

	# Start by fixing the positions of the main spine
	print('Fixing spline....')
	KinChain = [x1,x2,x3,x4,x5,x6,x7,x8,x9];
	fixed_spline_values = FABRIK_marker_correction(KinChain, 9)


	# Fix front left leg
	if('Lizard:UpperBack' in marker_names):
		fl1 = markers['Lizard:UpperBack']
	else:
		fl1 = markers['Lizard:UpperBody']
	fl2 = markers['Lizard:FrontLeftLeg']
	fl3 = markers['Lizard:FrontLeftFoot']

	print('Fixing front left leg....')
	KinChainFrontLeft = [fl1, fl2, fl3]
	fixed_left_leg = FABRIK_marker_correction_leg(KinChainFrontLeft, 3)

	# Fix front right leg
	if('Lizard:UpperBack' in marker_names):
		fr1 = markers['Lizard:UpperBack']
	else:
		fr1 = markers['Lizard:UpperBody']
	fr2 = markers['Lizard:FrontRightLeg']
	fr3 = markers['Lizard:FrontRightFoot']

	print('Fixing front right leg....')
	KinChainFrontRight = [fr1, fr2, fr3]
	fixed_right_leg = FABRIK_marker_correction_leg(KinChainFrontRight, 3)

	# Fix back right leg
	if('Lizard:LowerBack' in marker_names):
		br1 = markers['Lizard:LowerBack']
	else:
		br1 = markers['Lizard:LowerBody']
	br2 = markers['Lizard:BackRightLeg']
	br3 = markers['Lizard:BackRightFoot']

	print('Fixing back right leg....')
	KinChainBackRight = [br1, br2, br3]
	fixed_back_right_leg = FABRIK_marker_correction_leg(KinChainBackRight, 3)

	# Fix back left leg
	if('Lizard:LowerBack' in marker_names):
		bl1 = markers['Lizard:LowerBack']
	else:
		bl1 = markers['Lizard:LowerBody']
	bl2 = markers['Lizard:BackLeftLeg']
	bl3 = markers['Lizard:BackLeftFoot']

	print('Fixing back left leg....')
	KinChainBackLeft = [bl1, bl2, bl3]
	fixed_back_left_leg = FABRIK_marker_correction_leg(KinChainBackLeft, 3)

	# Create a new c3d file
	# This file should contain the following c3d-marker names
	new_marker_names = [
		'Lizard:Head',
		'Lizard:Neck',
		'Lizard:FrontRightLeg',
		'Lizard:FrontRightFoot',
		'Lizard:FrontLeftLeg',
		'Lizard:FrontLeftFoot',
		'Lizard:UpperBack',
		'Lizard:LowerBack',
		'Lizard:BackRightLeg',
		'Lizard:BackRightFoot',
		'Lizard:BackLeftLeg',
		'Lizard:BackLeftFoot',
		'Lizard:Tail0',
		'Lizard:Tail1',
		'Lizard:Tail2',
		'Lizard:Tail3',
		'Lizard:Tail4'
	]

	# In our c3d files some of the names may differ a little
	# Make a small name convertion according to the scenario
	if('Lizard:UpperBody' in marker_names):
		new_marker_names[6] = 'Lizard:UpperBody'
		new_marker_names[7] = 'Lizard:LowerBody'

	# Map the correct fixed values with the above marker name values
	new_fixed_values = [
		fixed_spline_values[0],
		fixed_spline_values[1],
		fixed_right_leg[1],
		fixed_right_leg[2],
		fixed_left_leg[1],
		fixed_left_leg[2],
		fixed_spline_values[2],
		fixed_spline_values[3],
		fixed_back_right_leg[1],
		fixed_back_right_leg[2],
		fixed_back_left_leg[1],
		fixed_back_left_leg[2],
		fixed_spline_values[4],
		fixed_spline_values[5],
		fixed_spline_values[6],
		fixed_spline_values[7],
		fixed_spline_values[8],
	]

	# Using the c3d server library create a new c3d file with fixed positions
	print('Writing values to a new c3d file....')
	for i in range(0, 17):
		tgt_mkr_data = c3d.get_marker_data(itf, new_marker_names[i], blocked_nan=False)
		tgt_mkr_resids = tgt_mkr_data[:, 3]
		for j in range(0, len(tgt_mkr_data)):    
			tgt_mkr_resids[j] = 0.0    
		
		c3d.set_marker_pos(itf, new_marker_names[i], new_fixed_values[i], None, None)
		print(c3d.set_marker_resid(itf, new_marker_names[i], np.zeros(len(new_fixed_values[i])), None, None, True))

	ret = c3d.save_c3d(itf, 'new_' + args.c3d_file)

	# Close the C3D file from C3Dserver
	ret = c3d.close_c3d(itf)

main()